![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* A curated collection of [links](https://bitbucket.org/imhicihu/experimental-inner-projects/src/master/links_collected.md), data, github repos, snippets, ideas that can be used in creative ways. 
* The scope of this repository begins and ends with the former idea proposed.
* This repo is a living document that will grow and adapt over time. We welcome your ideas, links and point of views

![note-taking.png](https://bitbucket.org/repo/R9Xpo7y/images/2725558601-note-taking.png)

### What is this repository for? ###

* Quick summary
    - A myriad of [links](https://bitbucket.org/imhicihu/experimental-inner-projects/src/master/links_collected.md), github [repos](https://bitbucket.org/imhicihu/experimental-inner-projects/src/master/links_collected.md) and [snippets](https://bitbucket.org/imhicihu/workspace/snippets/) for quick access. A kind of [Evernote](https://evernote.com/) (plan B) or _quick-reminder_/_mindmaps_ to check.
* Version 1.01

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/experimental-inner-projects/src)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/experimental-inner-projects/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - Please, any inquiry or point of view can be post in our [board](https://bitbucket.org/imhicihu/experimental-inner-projects/addon/trello/trello-board)
* Code review
    - Any hint or pull request are welcome
* Other guidelines
    - There is no other guidelines up to now

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/experimental-inner-projects/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/experimental-inner-projects/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)
